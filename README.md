# Tâches effectuées par l'équipe

## Création d'un fichier .gitlab-ci.yml
## Création d'un rapport de test à l'aide de JACOCO
   
Les tests (déja existants) sont lancés grâce à Maven & Jacoco, et un rapport HTML est généré.

Suite à cela, le rapport est hébergé sur une [GitLab Page](https://zarhkoh.gitlab.io/Brick-Game-9999-in-1/)!

Le code coverage est aussi récupéré pour pouvoir être suivi depuis GitLab [(Analytics > repository)](https://gitlab.com/Zarhkoh/Brick-Game-9999-in-1/-/graphs/master/charts)

Nous pouvons donc afficher ce badge qui s'actualise automatiquement à chaque nouveau build réussi: 

[![coverage report](https://gitlab.com/Zarhkoh/Brick-Game-9999-in-1/badges/master/coverage.svg)](https://zarhkoh.gitlab.io/Brick-Game-9999-in-1/)


## Sauvegarde d'artefact dans Packages & Registries

Afin d'enregistrer un artefact dans Packages & Registries, nous avons ajouté un fichier "ci_settings.xml" faisant appelle à un serveur fourni par Gitlab.
Dans ce même fichier, nous avons donné un ID, et cette ID doit être similaire dans le "pom.xml".

ci_settings.xml :
```
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Job-Token</name>
            <value>${env.CI_JOB_TOKEN}</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```
Et dans le pom.xml, on set nos variables (pour nous, nous avons laissé celle par défaut de Gitlab):
```
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
  </repository>
</repositories>
<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```
Et pour terminer, on ajoute cela dans le fichier gitlab-ci.yml afin qu'il puisse exécuter la sauvegarde à chaque fois sur la master :
```
deploy:
  image: maven:3.6-jdk-11
  stage : test-registry
  script:
    - 'mvn deploy -s ci_settings.xml'
  only:
    - master
```
source : https://docs.gitlab.com/ee/user/packages/maven_repository/#create-maven-packages-with-gitlab-cicd

# README de Brick-Game-9999-in-1

## Brick Game 9999 in 1 

This project is simple simulator of famous Brick Game handheld retro gaming console. 

### Installation

`mvn clean package`

launch game

`java -jar brick-game-0.3.0-SNAPSHOT.jar`

### Usage

##### Control keys

- Default keys (independent of the game) 
    - `Key 'P'` - On/Off **P**ause
    - `Key 'S'` - On/Off **S**ound
    - `Key 'R'` - **R**eset the brick game
- Functional keys
    - `Key Up` - change speed in menu or specific in game
    - `Key Down` - change level in menu or specific in game
    - `Key Left` - decrease the number of game in menu or specific in game
    - `Key Right` - increase the number of game in menu or specific in game
    - `Key Space` - choose game in menu or specific in game

##### View

![](https://raw.githubusercontent.com/vitalibo/Brick-Game-9999-in-1/assets/docs/img/sR35V1.gif) ![](https://raw.githubusercontent.com/vitalibo/Brick-Game-9999-in-1/assets/docs/img/IFREtC.gif) ![](https://raw.githubusercontent.com/vitalibo/Brick-Game-9999-in-1/assets/docs/img/bUdlw7.gif) ![](https://raw.githubusercontent.com/vitalibo/Brick-Game-9999-in-1/assets/docs/img/x82Vbe.gif)

![](https://raw.githubusercontent.com/vitalibo/Brick-Game-9999-in-1/assets/docs/img/AwX9jY.gif) ![](https://raw.githubusercontent.com/vitalibo/Brick-Game-9999-in-1/assets/docs/img/ar3crF.gif) 

- Board panel `capacity [20x10]`
- Preview panel `capacity [4x4]`
- Score `max 999999`
- Speed `max 15`
- Level `max 15`
- Sound `on/off`
- Pause `on/off`

### Games
- `0001` Snake
- `0002` Race
- `0003` Tetris
- `0004` Shoot
- `0005` Tanks
